from django.conf import settings
from local_devices.models import LocalDevice
from devices_policies.models import MacAddress
from django.core.mail import send_mail
from django.template.loader import render_to_string
from networkFunctions import find_active_network_interface, find_my_local_ip

def check_mac_address_exists(macAddresses):
    macs = macAddresses.copy()
    for mac in macs:
        try:
            object = MacAddress.objects.using('mysqlDB').get(mac_address=mac)
            if(LocalDevice.objects.filter(mac_address=object.mac_address).exists()):
                pass
            else:
                active_interface = find_active_network_interface()
                print("Macs: {}".format(macs))
                local_ip = find_my_local_ip(active_interface)
                print("Local Ip: {}".format(local_ip))

                local_device = LocalDevice.objects.create(mac_address=object.mac_address)
                device = local_device.get_device_details()
                msg_plain = render_to_string('email.txt',
                                             {'local_device': local_device,
                                             'device':device,
                                             'local_ip':local_ip})
                msg_html = render_to_string('email.html',
                                             {'local_device': local_device,
                                             'device':device,
                                             'local_ip':local_ip})
                send_mail('New Device Added', msg_plain, 'gkaframanis@gmail.com', ['gregotel7@gmail.com'], html_message=msg_html, fail_silently=True,)
        except MacAddress.DoesNotExist:
            print("Mac does not exist")
            pass