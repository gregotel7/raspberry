from django.db import models


class Policy(models.Model):
    registrator_id = models.PositiveIntegerField()
    perm_dest_ports = models.CharField(max_length=255)
    perm_dest_ips = models.CharField(max_length=512)
    max_size_packet = models.PositiveIntegerField()
    max_data_rate = models.PositiveIntegerField()
    starting_time = models.TimeField()
    ending_time = models.TimeField()
    restriction_level = models.CharField(max_length=1, default="0")

    class Meta:
        managed = False
        db_table = 'Policy'

    
class Device(models.Model):
    registrator_id = models.PositiveIntegerField()
    manufacturer = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    slug = models.CharField(max_length=255)
    website = models.URLField(max_length=255)
    policies = models.ManyToManyField('Policy', related_name='devices')
    
    class Meta:
        managed = False
        db_table = 'Device'
    

class MacAddress(models.Model):
    registrator_id = models.PositiveIntegerField()
    mac_address = models.CharField(unique=True, max_length=17)
    device = models.ForeignKey('Device', on_delete=models.CASCADE, related_name='macAddresses')

    class Meta:
        managed = False
        db_table = 'MacAddress'
        
    

    
    
    
    
    
