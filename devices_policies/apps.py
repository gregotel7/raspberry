from django.apps import AppConfig


class DevicesPoliciesConfig(AppConfig):
    name = 'devices_policies'
