import re
import subprocess

def find_active_network_interface() -> str:
    p1 = subprocess.Popen(["ip", "route", "get", "1.1.1.1"], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["grep", "-Po", r"(?<=dev\s)\w+"], stdin=p1.stdout, stdout=subprocess.PIPE)
    p1.stdout.close()
    p3 = subprocess.Popen(["cut", "-f1", "-d", " "], stdin=p2.stdout, stdout=subprocess.PIPE)
    p2.stdout.close()
    interface_output = (p3.communicate()[0]).strip(b"\n")
    p3.stdout.close()
    interface_output = str(interface_output, "UTF-8")
    interface = interface_output
    return interface


def find_my_local_ip(interface: str) -> str:
    p1 = subprocess.Popen(["ip", "addr", "show", "{}".format(interface)], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["grep", "inet "], stdin=p1.stdout, stdout=subprocess.PIPE)
    p1.stdout.close()
    p3 = subprocess.Popen(["cut", "-d", "/", "-f1"], stdin=p2.stdout, stdout=subprocess.PIPE)
    p2.stdout.close()
    p4 = subprocess.Popen(["cut", "-d", " ", "-f6"], stdin=p3.stdout, stdout=subprocess.PIPE)
    p3.stdout.close()
    # interface is a byte object
    local_ip = p4.communicate()[0]
    p4.stdout.close()
    # Convert the bytes object to string
    local_ip = str(local_ip, "UTF-8")
    local_ip = local_ip.rstrip("\n")
    return local_ip



