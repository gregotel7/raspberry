from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='homepage'),
    path('devices/<int:pk>/', views.LocalDeviceDetail.as_view(), name='device_detail'),
    path('devices/<int:pk>/select_policy/',views.LocalDeviceSelectPolicy.as_view(), name='device_select_policy'),
    path('devices/<int:pk>/policies/', views.LocalDevicePolicyList.as_view(), name='device_policy_list'),
    path('devices/<int:pk>/policy/<int:policy_pk>/', views.LocalDevicePolicyDetail.as_view(), name='device_policy_detail'),
    path('devices/', views.LocalDeviceListView.as_view(), name='device_list'),
]