from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.views.generic import View, TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse
from .models import LocalDevice
from devices_policies.models import Policy
from iptablesFunctions import populateIptables

class HomePageView(TemplateView):
    template_name = 'homepage.html'

    
class LocalDeviceListView(ListView):
    model = LocalDevice
    template_name = 'local_devices/device_list.html'
    context_object_name = 'local_device_list'
    
    
class LocalDeviceDetail(View):
    def get(self, request, pk):
        local_device = LocalDevice.objects.get(id=pk)
        device = local_device.get_device_details()
        policies = local_device.get_device_policies()
        return render(request, 'local_devices/device_detail.html',
                      {'local_device':local_device, 'device': device, 'policies':policies})
    model = LocalDevice
    template_name = 'local_devices/device_detail.html'
    context_object_name = 'local_device'


class LocalDevicePolicyList(View):
    def get(self, request, pk):
        local_device = LocalDevice.objects.get(id=pk)
        device = local_device.get_device_details()
        policies = local_device.get_device_policies()
        return render(request, 'local_devices/device_policy_list.html',
                      {'device': device, 'policies':policies})


class LocalDevicePolicyDetail(View):
    def get(self, request, pk, policy_pk):
        local_device = LocalDevice.objects.get(id=pk)
        device = local_device.get_device_details()
        policy = Policy.objects.using('mysqlDB').get(id=policy_pk)
        return render(request, 'local_devices/device_policy_detail.html',
                      {'device': device, 'policy':policy})

        
class LocalDeviceSelectPolicy(View):
    def get(self, request, pk):
        local_device = LocalDevice.objects.get(id=pk)
        device = local_device.get_device_details()
        policies = local_device.get_device_policies()
        return render(request, 'local_devices/device_select_policy.html',
                      {'device': device, 'policies':policies})
        
    def post(self, request, pk):
        local_device = LocalDevice.objects.get(id=pk)
        device = local_device.get_device_details()
        policies = local_device.get_device_policies()
        local_device.selected_policy = request.POST['selected_policy']
        local_device.save()
        populateIptables()
        return HttpResponseRedirect(reverse('device_detail', args=(pk,)))