from django.apps import AppConfig


class LocalDevicesConfig(AppConfig):
    name = 'local_devices'
