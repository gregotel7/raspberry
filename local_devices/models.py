from django.db import models
from django.urls import reverse

from devices_policies.models import Device, Policy, MacAddress


class LocalDevice(models.Model):
    mac_address = models.CharField(max_length=17, unique=True)
    selected_policy = models.IntegerField(default=0)
    
    # A Device object
    device = Device()
    policies = []
    
    def __str__(self):
        return self.mac_address
    
    # Get the Device details from the external db based on the the MAC address stored at the default db.
    def get_device_details(self):
        mac_object = MacAddress.objects.using('mysqlDB').select_related('device').get(mac_address=self.mac_address)
        self.device = mac_object.device
        return self.device
    
    def get_device_policies(self):
        self.device = self.get_device_details()
        self.policies = self.device.policies.using('mysqlDB').all()
        return self.policies
 
    def get_absolute_url(self):
        return reverse('device_detail', args=[str(self.pk)])
    
    
    
    
    
