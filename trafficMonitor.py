import os
# Configure settings for project
# Need to run this before calling models from application!
os.environ.setdefault('DJANGO_SETTINGS_MODULE','raspberry.settings')

import django
# Import settings
django.setup()

import pyshark
import checkDevices
import iptablesFunctions
import threading
import time, traceback

macAddresses = set()

def macaddress_check_set(pkt: pyshark.packet.packet.Packet):
    if pkt['eth'].get_field_value('src') not in macAddresses:
        macAddresses.add(pkt['eth'].get_field_value('src'))
        
def every(delay, task, task_arg):
  next_time = time.time() + delay
  while True:
    time.sleep(max(0, next_time - time.time()))
    try:
      task(task_arg)
    except Exception:
      traceback.print_exc()
    next_time += (time.time() - next_time) // delay * delay + delay

checkDevices.check_mac_address_exists(macAddresses)

if __name__ == '__main__':
    iptablesFunctions.populateIptables()
    capture = pyshark.LiveCapture(interface='wlan0')
    capture.sniff_continuously()
    threading.Thread(target=lambda: every(10, checkDevices.check_mac_address_exists, macAddresses)).start()
    capture.apply_on_packets(macaddress_check_set)
    capture.close()
    
