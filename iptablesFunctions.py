from devices_policies.models import Device, Policy, MacAddress
from local_devices.models import LocalDevice

import iptc
import datetime


def populateIptables():

    chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "FORWARD")
    # iptables -F, --flush [chain_name]
    chain.flush()
    chain.set_policy("DROP")
    if LocalDevice.objects.all():
        for local_device in LocalDevice.objects.all():
            # eg: iptables -A chain_name -m mac --mac-source 00:00:00:00:00:01
            mac_address = local_device.mac_address
            policy_pk = local_device.selected_policy
            policy = Policy.objects.using('mysqlDB').get(id=policy_pk)

            rule = iptc.Rule()
            rule.out_interface="eth0"
            match = iptc.Match(rule, "mac")
            match.mac_source = mac_address
            rule.add_match(match)

            if policy.perm_dest_ips:
                # -d, --dst, --destination
                destination_ips = policy.perm_dest_ips
                # Remove blank spaces
                destination_ips = destination_ips.replace(" ","")
                # Split the IP Addresses where the commas are.
                ip_addresses_list = destination_ips.split(",")

                for ip_address in ip_addresses_list:
                    rule.dst = ip_address   

                    if policy.perm_dest_ports:
                        # -m multiport --destination-port 21,23,80
                        rule.protocol = "tcp"
                        match = iptc.Match(rule, "multiport")
                        destination_ports = policy.perm_dest_ports
                        # Remove blank spaces
                        destination_ports = destination_ports.replace(" ","")
                        match.dports = destination_ports
                        rule.add_match(match)

                    if policy.max_size_packet:
                        # -m length --length 1400:1500
                        match = iptc.Match(rule, "length")
                        max_packet_size = policy.max_size_packet
                        match.length = str(max_packet_size)
                        rule.add_match(match)

                    if policy.max_data_rate:
                        # -m limit --limit
                        match = iptc.Match(rule, "limit") 
                        max_data_rate = policy.max_data_rate
                        match.limit = str(max_data_rate)
                        rule.add_match(match)

                    if policy.starting_time and policy.ending_time:
                        # iptables RULE -m time --timestart TIME --timestop TIME --days DAYS-j ACTION
                        match = iptc.Match(rule, "time")
                        starting_time = policy.starting_time.strftime('%H:%M')
                        ending_time = policy.ending_time.strftime('%H:%M')
                        match.timestart = starting_time
                        match.timestop = ending_time
                        rule.add_match(match)

                    rule.target = iptc.Target(rule, "ACCEPT")

                    chain.insert_rule(rule)
            
            else:   
                if policy.perm_dest_ports:
                    # -m multiport --destination-port 21,23,80 (page 209)
                    rule.protocol = "tcp"
                    match = iptc.Match(rule, "multiport")
                    destination_ports = policy.perm_dest_ports
                    # Remove blank spaces
                    destination_ports = destination_ports.replace(" ","")
                    match.dports = destination_ports
                    rule.add_match(match)

                if policy.max_size_packet:
                    # -m length --length 1400:1500 (page 204)
                    match = iptc.Match(rule, "length")
                    max_packet_size = policy.max_size_packet
                    match.length = str(max_packet_size)
                    rule.add_match(match)

                if policy.max_data_rate:
                    # -m limit --limit
                    match = iptc.Match(rule, "limit") 
                    max_data_rate = policy.max_data_rate
                    match.limit = str(max_data_rate)
                    rule.add_match(match)

                if policy.starting_time and policy.ending_time:
                    # iptables RULE -m time --timestart TIME --timestop TIME --days DAYS-j ACTION
                    match = iptc.Match(rule, "time")
                    starting_time = policy.starting_time.strftime('%H:%M')
                    ending_time = policy.ending_time.strftime('%H:%M')
                    match.timestart = starting_time
                    match.timestop = ending_time
                    rule.add_match(match)

                rule.target = iptc.Target(rule, "ACCEPT")

                chain.insert_rule(rule)
    else:
        pass


